import { SALVAGE } from './module/config.js'
import SalvageUnionActor from './module/SalvageUnionActor.js'
import SalvageUnionItem from './module/SalvageUnionItem.js'
import SalvageUnionItemSheet from './module/sheets/SalvageUnionItemSheet.js'
import SalvageUnionChassisItemSheet from './module/sheets/SalvageUnionChassisItemSheet.js'
import SalvageUnionCrawlerBayItemSheet from './module/sheets/SalvageUnionCrawlerBayItemSheet.js'
import SalvageUnionActorSheet from './module/sheets/SalvageUnionActorSheet.js'
import SalvageUnionNPCActorSheet from './module/sheets/SalvageUnionNPCActorSheet.js'
import SalvageUnionJournalSheet from './module/sheets/SalvageUnionJournalSheet.js'
import {
  preloadHandlebarsTemplates,
  registerHandlebarsHelpers,
  registerSystemSettings,
  setupEnrichers,
} from './module/preload.js'
import * as Migrations from './module/migrate.js'
import * as Chat from './module/chat.js'
import { rollTable, heatRoll } from './module/dice.js'

Hooks.once('init', function () {
  console.log(
    'salvage-union | Initialising Salvage Union (Unofficial) system...',
  )
  // Register system settings
  registerSystemSettings()
  setupEnrichers()

  CONFIG.SALVAGE = SALVAGE
  CONFIG.Item.documentClass = SalvageUnionItem
  CONFIG.Item.typeIcons = {
    equipment: 'fas fa-screwdriver-wrench',
    ability: 'fas fa-person-running fa-flip-vertical',
    'npc-ability': 'fas fa-person-running fa-flip-vertical',
    chassis: 'fas fa-robot',
    system: 'fas fa-gear',
    module: 'fas fa-microchip',
    salvage: 'fas fa-gears',
    'crawler-bay': 'fas fa-pipe-valve',
  }

  CONFIG.Actor.documentClass = SalvageUnionActor
  CONFIG.Actor.typeIcons = {
    pilot: 'fas fa-user-headset',
    mech: 'fas fa-user-robot',
    'union-crawler': 'fa-solid fa-conveyor-belt-boxes',
    npc: 'fas fa-user',
    'npc-mech': 'fas fa-user-robot',
    'bio-titan': 'fas fa-spaghetti-monster-flying',
    vehicle: 'fas fa-truck-pickup',
  }

  if (game.settings.get('salvage-union', 'customStatuses')) {
    CONFIG.statusEffects = SALVAGE.statusEffects
  }

  CONFIG.SALVAGE.crawlerUpgradeScrapRequired = game.settings.get(
    'salvage-union',
    'crawlerUpgradeScrapRequired',
  )

  game.salvage = {
    heatRoll: heatRoll,
  }

  // Item sheets
  Items.registerSheet('salvage-union', SalvageUnionItemSheet, {
    makeDefault: true,
  })
  Items.registerSheet('salvage-union', SalvageUnionChassisItemSheet, {
    makeDefault: true,
    types: ['chassis'],
  })
  Items.registerSheet('salvage-union', SalvageUnionCrawlerBayItemSheet, {
    makeDefault: true,
    types: ['crawler-bay'],
  })
  Items.unregisterSheet('core', ItemSheet)

  // Actor sheets
  Actors.registerSheet('salvage-union', SalvageUnionActorSheet, {
    makeDefault: true,
    types: ['pilot', 'mech', 'union-crawler', 'bio-titan'],
  })
  Actors.registerSheet('salvage-union', SalvageUnionNPCActorSheet, {
    makeDefault: true,
    types: ['npc', 'npc-mech', 'vehicle'],
  })
  Actors.unregisterSheet('core', ActorSheet)

  // Journal sheet
  Journal.registerSheet('salvage-union', SalvageUnionJournalSheet, {
    makeDefault: true,
  })

  // Preload templates
  preloadHandlebarsTemplates()

  // Handlebar helpers
  registerHandlebarsHelpers()

  // TinyMCE styling
  CONFIG.TinyMCE.content_css = 'systems/salvage-union/styles/tinymce.css'
  CONFIG.TinyMCE.style_formats.push({
    title: 'Salvage Union',
    items: [{ title: 'Condensed', inline: 'span', classes: 'condensed' }],
  })
})

Hooks.once('ready', () => {
  if (!game.user.isGM) return

  const currentVersion = game.settings.get(
    'salvage-union',
    'systemMigrationVersion',
  )
  const NEEDS_MIGRATION_VERSION = '0.2.4'

  const needsMigration =
    !currentVersion ||
    foundry.utils.isNewerVersion(NEEDS_MIGRATION_VERSION, currentVersion)

  if (needsMigration) {
    Migrations.migrateWorld()
    game.settings.set(
      'salvage-union',
      'systemMigrationVersion',
      NEEDS_MIGRATION_VERSION,
    )
  }
})

Hooks.on('renderSettings', (_app, html) => {
  const url =
    'https://gitlab.com/pacosgrove1/salvage-union-foundry-vtt-unofficial/-/wikis/home'
  const label = game.i18n.localize('salvage-union.common.documentation')

  let helpButton = $(
    `<button id="salvage-union-help-btn" data-action="salvage-union-help">
      <i class="fas fa-gear"></i> ${label}
    </button>`,
  )
  html.find('button[data-action="support"]').before(helpButton)

  helpButton.on('click', (event) => {
    event.preventDefault()
    window.open(url, 'salvage-union-help', 'width=1032,height=720')
  })
})

Hooks.on('renderChatMessage', Chat.onRenderChatMessage.bind(this))

Hooks.on('renderJournalPageSheet', (_obj, html) => {
  html.on('click', '.table-roll', (event) => {
    const data = event.currentTarget.dataset
    const tableId = data.tableId

    rollTable(tableId)
  })
})

Hooks.once('diceSoNiceReady', (dice3d) => {
  const commonDice = {
    category: 'Salvage Union',
    foreground: '#FFFFFF',
    outline: '#000000',
    visibility: 'visible',
    font: 'Roboto Regular',
    fontScale: { d20: '0.75' },
    material: 'metal',
    texture: 'metal',
  }

  dice3d.addColorset(
    {
      ...commonDice,
      name: 'salvage-orange',
      description: 'Union Orange',
      background: '#f68a50',
      edge: '#f68a50',
    },
    'default',
  )

  dice3d.addColorset(
    {
      ...commonDice,
      name: 'salvage-blue',
      description: 'Corpo Blue',
      background: '#6bcef3',
      edge: '#6bcef3',
    },
    'default',
  )

  dice3d.addSystem(
    {
      id: 'salvage-union',
      name: 'Salvage Union',
    },
    'preferred',
  )

  dice3d.addDicePreset({
    type: 'd20',
    labels: [
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      '11',
      '12',
      '13',
      '14',
      '15',
      '16',
      '17',
      '18',
      '19',
      'systems/salvage-union/images/d20.webp',
    ],
    bumpMaps: [
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      ,
      'systems/salvage-union/images/d20-b.webp',
    ],
    system: 'salvage-union',
  })
})
