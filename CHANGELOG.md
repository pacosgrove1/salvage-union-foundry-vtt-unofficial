# Changelog

## v1.2.3

- Attempt to fix update loop

## v1.2.2

- Fix spelling error in Core Mechanic macro that prevented DiceSoNice triggering
- Mark as verified for v12

## v1.2.1

- Fix spelling error in Tech Level selectOptions

## v.1.2.0

- Add FoundryVTT v12 support

## v1.1.5

- Display Item uses when no other traits exist (#34)

## v1.1.4

- Allow Pilot Actors to switch from HP to SP (#31)
- Display full description on NPC sheets and in chat (#32)

## v1.1.3

- Add `SalvageUnionItem#inlineAbilities()` method to parse out the name and cost of abilities within an item's description

## v1.1.2

- Add an inline roll button when an Item with a RollTable is collapsed
- Fix chassis calculation on NPC mechs

## v1.1.1

- Fix Chassis stat application

## v1.1.0

- Adds dialog for spending AP/EP when the Cost is clicked from a Pilot or Mech sheet
  - Send chat message on confirm
- Track which Items on a sheet have been collapsed
- Add `uses` fields to various Items to replace the `Uses (X)` custom Traits
  - On an Actor sheet, click on the trait to decrease the remaining uses, right-click to reset
- Adds "BIOTECH" tech level for We Were Here First items
- Adds "NANITE" tech level for False Flag items
- Replaces reference to `ep` with `energy-points` in `SalvageUnionActor#updateMaximums`
- Moves some CSS around to make it more widely useful

## v1.0.9

- Update InlineNpc enricher to include chassis details for mechs
- Update compendium entries
  - [Pilot Equipment] Improvised Explosive Device description
  - [Pilot Equipment] Portable Comms Unit description
  - [Mech Module] Metal Detector
- Fix disappearing Mech stats on descended document modification
- Better handling of empty `<p>` tags around enrichers

## v1.0.8

- Fix HTML enrichment on Union Crawler

## v1.0.7

- Add checkboxes to track usage of Pilot Background/Motto/Keepsake
- Multi-line fields for pilot/mech profile data
- Collapsible item descriptions

## v1.0.6

- Add custom enricher for Chassis stat display
- Add custom enricher for NPC display
- Add banners for Compendium packs
- Styling and Compendium fixes

## v1.0.5

- Update Compendium data to match the v1.2 PDF
- Update the InlineItem and {cost} enricher
- Use enrichers across all item and actor descriptions

## v1.0.4

- Set inventory slot size manually for most Items
- Custom Token Effect Statuses
- Compendium fixes

## v1.0.3

- TextEditor enricher for better display of inline RollTables within Journals.
- Updates Journals to use new enricher.
- Fix chat message context menu
- Update default image assignment for Actors and Items

## v1.0.2

- Fix Compendium Journal links

## v1.0.1

- Fix pack inclusion in CI build

## v1.0.0

- Combine Crawler Bay and Crawler NPC Items
  - **Note:** Crawler NPCs will be removed in a future update
- Remove FoundryVTT v10 comptibility
- Update Compendiums to v11 format, with native folders
- New experimental option to allow moving of items between Actors
  - Dragging an item from one sheet to another will also remove it from the source Actor
- Multiple Traits can now be entered at once on supported sheets
  - Separated by `,` or `//`

## v0.2.7

- Rework Item/Actor ownership permissions
- Fix inventory overflow display

## v0.2.6

- FoundryVTT v11 compatibility
- Add buttons to roll Pilot Background, Motto and Keepsake
- Add Mech Quirk and Appearance to sheet, with roll buttons
- Add Union Crawler Upkeep and Upgrade tracking
- Improve Crawler salvage management
- Journal and translation fixes

## v0.2.5

- Improved Bio-Titan, Vehicle and NPC sheets
- Improved Inventory management for Pilots and Mechs
- Added compendia of Chassis, Systems, Modules, Pilot Abilities, Equipment, etc.
- Journal styling

## v0.2.4

- Add Bio-Titan Actor
- Add Tagging for Items
- Bug fixes around RollableTables attached from a Compendium

## v0.2.3

- No functionality changes, just code cleanup and some icon colour adjustment.

## v0.2.2

- Add functionality to adjust Crawler NPC HP from the Crawler sheet
- Add `clamp` helper method
- Use arrow functions for helpers

## v0.2.1

- Fix mech Chassis sheet
- Correctly display Chassis Ability on owner Mech sheet
- Enrich HTML for item descriptions to ensure dropped Document links work

## v0.2.0

- Updated for FoundryVTT v10
- System changes based on Alpha WIP rulebook
