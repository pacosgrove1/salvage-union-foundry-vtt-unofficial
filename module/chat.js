import { wrapWithP } from './helpers.js'

/**
 * Send a basic label/value to the chat log
 * @param {String} label The field label
 * @param {String} value The field value
 * @returns
 */
export const fieldToChat = async (label, value) => {
  basicChatMessage(`<h3>${label}</h3>${wrapWithP(value)}`)
}

export const basicChatMessage = async (html) => {
  const messageTemplate = 'systems/salvage-union/templates/chat/wrapper.hbs'
  const templateContext = { content: wrapWithP(html) }
  const chatData = {
    user: game.user._id,
    speaker: ChatMessage.getSpeaker(),
    content: await renderTemplate(messageTemplate, templateContext),
  }
  ChatMessage.create(chatData)
}

/**
 * Callbacks for rendering a message
 * @param {ChatMessage} _message Message object (unused)
 * @param {HTMLElement} html Message HTML body
 * @param {Object} _data Message metadata (unused)
 * @returns
 */
export const onRenderChatMessage = async (_message, html, _data) => {
  if (html.find('.salvage-union')[0]) return null
  html.find('.message-content').wrapInner('<div class="salvage-union"></div>')
}
