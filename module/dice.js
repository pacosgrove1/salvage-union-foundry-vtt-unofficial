/**
 * Roll on the RollTable associated with an Item.
 * @param {SalvageUnionItem} item The item
 * @returns Promise
 */
export const rollItemTable = async (item) => {
  const table = item.system.table
  if (table) {
    const result = await table.draw({ displayChat: false })

    const messageTemplate =
      'systems/salvage-union/templates/chat/item-table-roll.hbs'
    const templateContext = {
      name: item.name,
      result: result.results[0],
      type: item.type,
      system: item.system,
      roll: result.roll,
      activeStatus: CONFIG.SALVAGE.statusTypes.ACTIVE,
    }

    const content = await renderTemplate(messageTemplate, templateContext)
    const chatData = buildRollChatData(result.roll, content)

    return ChatMessage.create(chatData)
  }
}

/**
 * Roll on a specified table
 * @param {String} tableId The table's UUID
 */
export const rollTable = async (tableId) => {
  const table = await fromUuid(tableId)
  if (table) {
    const result = await table.draw({ displayChat: false })
    const messageTemplate =
      'systems/salvage-union/templates/chat/item-table-roll.hbs'
    const templateContext = {
      name: table.name,
      system: { description: table.description },
      result: result.results[0],
      roll: result.roll,
    }
    const content = await renderTemplate(messageTemplate, templateContext)
    const chatData = await buildRollChatData(result.roll, content)
    ChatMessage.create(chatData)
  }
}

/**
 * Roll a d20 against an Actor's heat value
 * @param {Integer} value The target number to roll under
 * @returns Promise
 */
export const heatRoll = async (value) => {
  const roll = await new Roll('1d20').roll({ async: true })

  const result = roll.total
  const fail = result <= value

  const description = fail ? `heat-fail` : `heat-pass`
  const messageTemplate = 'systems/salvage-union/templates/chat/heatRoll.hbs'

  const templateContext = {
    name: `salvage-union.chat.heat-roll`,
    value: value,
    roll: roll,
    fail: fail,
    description: description,
    macro: game.packs
      .get('salvage-union.macros')
      .index.getName('Reactor Overload').uuid,
  }

  const content = await renderTemplate(messageTemplate, templateContext)
  const chatData = buildRollChatData(roll, content)

  return ChatMessage.create(chatData)
}

/**
 * Build common data for Roll messages in chat
 * @param {Roll} roll The Roll object
 * @param {String} content The HTML to display in the ChatMessage
 * @returns An Object to pass to ChatMessage.create()
 */
const buildRollChatData = (roll, content) => {
  return {
    user: game.user._id,
    speaker: ChatMessage.getSpeaker(),
    roll: roll,
    rolls: [roll],
    content: content,
    sound: CONFIG.sounds.dice,
    type: CONST.CHAT_MESSAGE_TYPES.ROLL,
  }
}
