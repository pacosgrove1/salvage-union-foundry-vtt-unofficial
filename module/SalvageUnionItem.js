export default class SalvageUnionItem extends Item {
  static getDefaultArtwork(data) {
    return {
      img: CONFIG.SALVAGE.defaultTokens[data.type],
    }
  }

  chatTemplate = 'systems/salvage-union/templates/chat/item.hbs'

  async prepareData() {
    super.prepareData()

    // Override traits
    // Order: Action, Range, Damage, ...Traits
    const traits = []

    if (this.system.action) {
      traits.push(
        game.i18n.localize(
          `salvage-union.sheets.items.action.${this.system.action}`,
        ),
      )
    }

    if (this.system.range) {
      const range = game.i18n.localize(
        `salvage-union.sheets.items.range.${this.system.range}`,
      )
      const rangeTrait = game.i18n.format(
        'salvage-union.sheets.items.range.trait',
        { range: range },
      )
      traits.push(rangeTrait)
    }

    if (this.system.damage) {
      traits.push(
        game.i18n.format('salvage-union.sheets.items.damage.trait', {
          damage: this.system.damage,
        }),
      )
    }

    // Include user-defined traits
    if (this.system.traits)
      traits.push(
        ...this.system.traits.sort((a, b) =>
          a.toLocaleLowerCase() > b.toLocaleLowerCase() ? 1 : -1,
        ),
      )

    this.system.combinedTraits = traits

    if (this.system.ep == 0) {
      this.system.ep = null
    }

    if (this.system.ap == 0) {
      this.system.ap = null
    }

    if (this.system.table_id) {
      this.system.table = await fromUuid(this.system.table_id)

      if (this.system.table) {
        this.system.tableResults = this.system.table.results.contents.sort(
          (a, b) => (a.range[0] < b.range[1] ? 1 : -1),
        )
      }
    }

    this.system.inventorySlots ||= 0

    if (['ability', 'npc-ability'].includes(this.type))
      this.system.status = CONFIG.SALVAGE.statusTypes.ACTIVE
  }

  /**
   * Look up an item's details and send it to the chat
   * @returns Promise
   */
  async toChat() {
    const chatData = {
      user: game.user._id,
      speaker: ChatMessage.getSpeaker(),
    }

    const owner_id = this.actor !== undefined ? this.actor.id : null

    const cardData = {
      item: this,
      activeStatus: CONFIG.SALVAGE.statusTypes.ACTIVE,
      owner: owner_id,
    }

    chatData.content = await renderTemplate(this.chatTemplate, cardData)
    return ChatMessage.create(chatData)
  }

  inlineAbilities() {
    const regex = /<h[\d]>{cost=(?<cost>[\dX]+[EAX]P)}\s*(?<name>.+)<\/h[\d]>/gm
    const matches = this.system.description.matchAll(regex)
    const abilities = []
    for (const match of matches) {
      abilities.push(match.groups)
    }
    return abilities
  }
}
