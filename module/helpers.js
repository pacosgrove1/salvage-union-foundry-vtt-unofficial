/**
 * Filter a collection of Items by type, and optionally sort them
 * @param {Array} collection The list of Items
 * @param {String} type The Type of item we're looking for
 * @param {Boolean} [sort] Whether the collection should be sorted (default false)
 * @returns The filtered, sorted list
 */
export const filterItems = (collection, type, sort = false) => {
  let items = collection.filter((element) => element.type === type)

  if (sort) {
    return items.sort((a, b) => (a.name < b.name ? -1 : 1))
  }

  return items.sort((a, b) => (a.sort < b.sort ? -1 : 1))
}

/**
 * Restrict a value to within a range
 * @param {Number} value The value to restrict
 * @param {Number} min The minimum acceptable value
 * @param {Number} max The maximum acceptable value
 * @returns The clamped value
 */
export const clamp = (value, max, min = 0) =>
  Math.min(max, Math.max(min, value))

/**
 * Add a Trait to a document's system object
 * @param {Document} document The Foundry Document to be updated
 * @param {String} trait The new Trait value
 */
export const addTrait = (document, trait) => {
  if (!trait) return

  const traits = document.system.traits || []
  splitTraits(trait).map((t) => traits.push(t.trim()))

  document.update({
    system: {
      traits: traits
        .filter(onlyUnique)
        .sort((a, b) =>
          a.toLocaleLowerCase() > b.toLocaleLowerCase() ? 1 : -1,
        ),
    },
  })
}

/**
 * Split a string into individual traits
 * @param {String} traits The list of traits
 * @returns The array of traits
 */
const splitTraits = (traits) =>
  traits
    .split('//')
    .flatMap((trait) => trait.split(','))
    .filter((trait) => trait !== '')

/**
 * A filter callback method
 * @param {String} value The value to search for
 * @param {The current index} index
 * @param {The array to search} array
 * @returns Boolean true or false
 */
const onlyUnique = (value, index, array) => array.indexOf(value) === index

/**
 * Remove a trait from a Document's list
 * @param {Document} document The Item or Actor whose traits to modify
 * @param {String} trait The trait to remove
 */
export const removeTrait = (document, trait) => {
  const traits = document.system.traits
  traits.splice(traits.indexOf(trait), 1)
  document.update({
    system: {
      traits: traits,
    },
  })
}

/**
 * Enrich a cost match
 * @param {Array} match
 * @param {Object} _opt
 * @returns String
 */
export const enrichCost = async (match, _opt) => {
  const cost = match[1]
  const container = document.createElement('span')
  const tooltip = game.i18n.localize('salvage-union.sheets.items.cost')
  container.innerHTML = `<span class="item-cost" data-cost="${cost}" data-tooltip="${tooltip}">${cost}</span>`
  return container
}

/**
 * Enrich an inline Item
 * @param {Array} match
 * @param {Object} _opt
 * @returns String HTML
 */
export const enrichInlineItem = async (match, _opt) => {
  const uuid = match[1]
  const item = await fromUuid(uuid)
  const itemName = match[2] ?? item.name

  const glue = ' // '
  const container = document.createElement('span')
  container.className = 'inline-item'

  let html = `@UUID[${uuid}]{${itemName}}`

  if (item) {
    if (['module', 'system', 'equipment'].includes(item.type)) {
      html += `<strong class="tech-tier mini margin-x" data-tooltip="${game.i18n.localize(
        'salvage-union.salvage.tech-level',
      )}">
        ${item.system.techLevel}
      </strong>`
    }

    if (['module', 'system'].includes(item.type)) {
      html += `<strong><span data-tooltip="${game.i18n.localize(
        'salvage-union.sheets.items.slots',
      )}">
        Δ${item.system.slots}
      </span> | <span data-tooltip="${game.i18n.localize(
        'salvage-union.sheets.items.salvage-value',
      )}">
        ¤${item.system.salvageValue}
      </span></strong>`
    }

    html += '<br />'

    if (item.type !== 'chassis') {
      if (item.system.ep) {
        html += ` <span class="item-cost" data-tooltip="${game.i18n.localize(
          'salvage-union.sheets.items.ep',
        )}">${item.system.ep}EP</span>`
      }
      if (item.system.ap) {
        html += ` <span class="item-cost" data-tooltip="${game.i18n.localize(
          'salvage-union.sheets.items.ap',
        )}">${item.system.ap}AP</span>`
      }
    }

    if (item.system.combinedTraits.length > 0) {
      const traits = [...item.system.combinedTraits]

      if (parseInt(item.system.uses?.max) > 0)
        traits.push(usesTraitDisplay(item.system.uses?.max, false))

      html += `${traits.join(glue)}`
    }
  }
  container.innerHTML = await TextEditor.enrichHTML(html, { async: true })
  return container
}

/**
 * Enrich a Chassis link
 * @param {Array} match
 * @param {Object} _opt
 * @returns String HTML
 */
export const enrichInlineChassis = async (match, _opt) => {
  const uuid = match[1]
  const chassis = await fromUuid(uuid)
  const chassisName = match[2] ?? chassis.name
  const container = document.createElement('div')

  let html = `@UUID[${uuid}]{${chassisName}}`

  if (chassis) {
    container.className = 'inline-chassis'
    const system = chassis.system

    system.enrichedDescription = await enrich(system.description)

    html += `${system.enrichedDescription}`

    const stats = [
      {
        value: system.sp,
        label: game.i18n.localize('salvage-union.sheets.actor.mech.structure'),
      },
      {
        value: system.ep,
        label: game.i18n.localize('salvage-union.sheets.actor.mech.energy-pts'),
      },
      {
        value: system.heat,
        label: game.i18n.localize('salvage-union.sheets.actor.mech.heat-cap'),
      },
      {
        value: system.systemSlots,
        label: game.i18n.localize(
          'salvage-union.sheets.actor.mech.system-slots',
        ),
      },
      {
        value: system.moduleSlots,
        label: game.i18n.localize(
          'salvage-union.sheets.actor.mech.module-slots',
        ),
      },
      {
        value: system.cargo,
        label: game.i18n.localize(
          'salvage-union.sheets.items.chassis.cargo-cap',
        ),
      },
    ]

    html += '<div class="stats">'
    stats.forEach((attribute) => {
      html += `<div class="stat">
        <div class="value">${attribute.value}</div>
        <div class="label">${attribute.label}</div>
      </div>`
    })
    html += '</stats>'
  }
  container.innerHTML = await TextEditor.enrichHTML(html, { async: true })
  return container
}

/**
 * Enrich an NPC link (NPC, Mech, BioTitan)
 * @param {Array} match
 * @param {Object} _opt
 * @returns String HTML
 */
export const enrichInlineNpc = async (match, _opt) => {
  const uuid = match[1]
  const actor = await fromUuid(uuid)
  const actorName = match[2] ?? actor.name

  const container = document.createElement('div')
  container.className = 'inline-npc item'

  let html = ''

  if (actor) {
    let healthType = '--'
    let healthMax = 0

    if (actor.system.sp) {
      healthType = 'salvage-union.sheets.actor.mech.structure'
      healthMax = actor.system.sp.max
    } else {
      healthType =
        actor.system.healthType === 'sp'
          ? 'salvage-union.sheets.actor.mech.structure'
          : 'salvage-union.sheets.actor.npc.hp'
      healthMax = actor.system.hp.max
    }

    html += `<h3>@UUID[${uuid}]{${actorName}}</h3>`
    if (actor.system.description) html += actor.system.description

    html += `<div class="stats">`
    html += `<div class="stat">
      <div class="value">${healthMax}</div>
      <div class="label">${game.i18n.localize(healthType)}</div>
    </div>`

    const chassis = actor.system.chassis
    if (chassis) {
      html += `<div class="stat">
        <div class="value">${chassis.system.ep}</div>
        <div class="label">${game.i18n.localize(
          'salvage-union.sheets.items.chassis.ep-max',
        )}</div>
      </div>`
      html += `<div class="stat">
        <div class="value">${chassis.system.heat}</div>
        <div class="label">${game.i18n.localize(
          'salvage-union.sheets.items.chassis.heat-max',
        )}</div>
      </div>`
      html += `<div class="stat">
        <div class="value">${chassis.system.systemSlots}</div>
        <div class="label">${game.i18n.localize(
          'salvage-union.sheets.actor.mech.system-slots',
        )}</div>
      </div>`
      html += `<div class="stat">
        <div class="value">${chassis.system.moduleSlots}</div>
        <div class="label">${game.i18n.localize(
          'salvage-union.sheets.actor.mech.module-slots',
        )}</div>
      </div>`
      html += `<div class="stat">
        <div class="value">${actor.system.inventory.max}</div>
        <div class="label">${game.i18n.localize(
          'salvage-union.sheets.items.chassis.cargo-cap',
        )}</div>
      </div>`
      html += `<div class="stat">
        <div class="value">${techLevelNumber(chassis.system.techLevel)}</div>
        <div class="label">${game.i18n.localize(
          'salvage-union.salvage.tech-level',
        )}</div>
      </div>`
      html += `<div class="stat">
        <div class="value">${chassis.system.salvageValue}</div>
        <div class="label">${game.i18n.localize(
          'salvage-union.sheets.items.salvage-value',
        )}</div>
      </div>`
    }
    html += `</div>`

    if (chassis) {
      html += `<h4 class="inverted">@UUID[${chassis.uuid}]</h4>`
      html += await enrich(chassis.system.description)
    }

    for (const item of actor.system.systems || []) {
      html += enrichedItem(item)
    }

    for (const item of actor.system.modules || []) {
      html += enrichedItem(item)
    }

    for (const item of actor.system.abilities || []) {
      html += enrichedItem(item)
    }

    for (const item of actor.system.equipment || []) {
      html += enrichedItem(item)
    }
  } else {
    html += `@UUID[${uuid}]{${actorName}}`
  }

  container.innerHTML = await TextEditor.enrichHTML(html, { async: true })
  return container
}

/**
 * Enrich an Item link
 * @param {Document} item
 * @returns String HTML
 */
const enrichedItem = (item) => {
  let html = `<h4 class="title">@UUID[${item.uuid}]{${item.name}}</h4>`
  if (item.system.combinedTraits) {
    const traits = [...item.system.combinedTraits]
    if (parseInt(item.system.uses?.max) > 0)
      traits.push(usesTraitDisplay(item.system.uses?.max, false))

    html += `<p><em>${traits.join(' // ')}</em></p>`
  }
  return html
}

/**
 * Enrich a DisplayTable link to show the contents
 * @param {string} match The matching UUID and display name
 * @param {Object} opt Addiitonal options (unused)
 * @returns The enriched table data
 */
export const enrichDisplayTable = async (match, asc = false) => {
  const uuid = match[1]
  const table = await fromUuid(uuid)
  const tableName = match[2] ?? table.name

  const container = document.createElement('div')
  container.classList.add('display-table')
  let html = `@UUID[${uuid}]{${tableName}}`

  if (table) {
    html = displayTable(table, tableName, asc)
    container.innerHTML = html
  }
  container.innerHTML = await TextEditor.enrichHTML(html, { async: true })
  return container
}

const displayTable = (table, tableName, asc = false) => {
  if (!table) return ''

  const results = table.results.contents.sort((a, b) =>
    asc ? (a.range[0] < b.range[1] ? -1 : 1) : a.range[0] < b.range[1] ? 1 : -1,
  )

  let html = `
    <h3>@UUID[${table.uuid}]{${tableName}}</h3>
    <p class="center">
      ${tableRollButton(table)}
    </p>
    <table class="table-results">
  `

  results.forEach((result) => {
    html += `
      <tr>
        <td>
          <strong>${displayRange(result.range)}</strong>
        </td>
        <td>
          ${result.text}
        </td>
      </tr>
    `
  })

  html += `
    </table>
  `
  return html
}

/**
 * Display a range
 * @param {Array} range The lower and upper limits of the range
 * @returns String Formatted range
 */
const displayRange = (range) => {
  if (range[0] === range[1]) return range[0]
  return '' + range[0] + '-' + range[1]
}

/**
 * Render a button to roll on a RollTable
 * @param {Document} table RollTable document
 * @returns String HTML
 */
const tableRollButton = (table) => {
  return `
    <a class="inverted table-roll" data-table-id="${table.uuid}">
      <strong class="all-caps">${game.i18n.localize(
        'salvage-union.common.roll',
      )}</strong>
    </a>
  `
}

/**
 * Enrich a HTML block
 * @param {String} html The HTML to be enriched
 * @param {Boolean} secrets Whether to render secret blocks
 * @returns
 */
export const enrich = async (html, secrets = false) => {
  if (html)
    return await TextEditor.enrichHTML(html, {
      secrets: secrets,
      async: true,
    })

  return html
}

/**
 * Wrap a string in <p> tags, unless it already has them
 * @param {String} value The value to wrap
 * @returns The wrapped string
 */
export const wrapWithP = (value) => {
  if (!value) return null
  if (value.includes('<p>')) return value
  else return `<p>${value}</p>`
}

const techLevelNumber = (techLevel) =>
  Object.keys(CONFIG.SALVAGE.techLevels).indexOf(techLevel) + 1

export const usesTraitDisplay = (uses, link = true) => {
  let html = ''
  if (link)
    html += `<a class="uses" data-tooltip="${game.i18n.localize(
      'salvage-union.sheets.items.uses.tooltip',
    )}">`

  html += game.i18n.format('salvage-union.sheets.items.uses.trait', {
    value: uses,
  })

  if (link) html += '</a>'
  return html
}
