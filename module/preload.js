import {
  enrichDisplayTable,
  enrichInlineItem,
  enrichCost,
  enrichInlineChassis,
  enrichInlineNpc,
  wrapWithP,
  usesTraitDisplay,
} from './helpers.js'

export const preloadHandlebarsTemplates = async () => {
  const templatePaths = [
    'systems/salvage-union/templates/partials/chassis-card.hbs',
    'systems/salvage-union/templates/partials/crawler-bay-npc.hbs',
    'systems/salvage-union/templates/partials/crawler-salvage.hbs',
    'systems/salvage-union/templates/partials/description-editor.hbs',
    'systems/salvage-union/templates/partials/inventory.hbs',
    'systems/salvage-union/templates/partials/item-card.hbs',
    'systems/salvage-union/templates/partials/item-list.hbs',
    'systems/salvage-union/templates/partials/item-rolltable.hbs',
    'systems/salvage-union/templates/partials/pilot-profile.hbs',
    'systems/salvage-union/templates/partials/pilot-stats.hbs',
    'systems/salvage-union/templates/partials/rolltable.hbs',
    'systems/salvage-union/templates/partials/salvage-card.hbs',
    'systems/salvage-union/templates/partials/salvage.hbs',
    'systems/salvage-union/templates/partials/traits.hbs',
    'systems/salvage-union/templates/sheets/items/_action_select.hbs',
    'systems/salvage-union/templates/sheets/items/_range_select.hbs',
  ]

  return loadTemplates(templatePaths)
}

export const registerHandlebarsHelpers = () => {
  Handlebars.registerHelper('range', (range) => {
    if (range[0] === range[1]) return range[0]
    return '' + range[0] + ' - ' + range[1]
  })

  Handlebars.registerHelper('trPath', (path, key) => path + '.' + key)
  Handlebars.registerHelper(
    'statusClass',
    (status) => CONFIG.SALVAGE.statusClasses[status],
  )
  Handlebars.registerHelper(
    'statusLabel',
    (status) => CONFIG.SALVAGE.statusLabels[status],
  )
  Handlebars.registerHelper('not', (condition) => !condition)
  Handlebars.registerHelper('arrayContains', (array, element) => {
    return array.includes(element)
  })
  Handlebars.registerHelper('join', (array, separator) => array.join(separator))
  Handlebars.registerHelper('inventoryItemHeight', (slots) => {
    slots = parseInt(slots)
    return `calc(${slots * 1.5}rem + ${(slots - 1) * 0.5}rem + ${slots * 2}px)`
  })
  Handlebars.registerHelper('times', (n, content) => {
    let result = ''
    for (let i = 0; i < n; i++) {
      content.data.index = i + 1
      result += content.fn(i)
    }
    return result
  })
  Handlebars.registerHelper('any', (array) => array.length > 0)
  Handlebars.registerHelper('crawlerUpkeepCost', (techLevel) =>
    game.i18n.format('salvage-union.sheets.actor.crawler.upkeep.cost', {
      level: techLevel,
    }),
  )
  Handlebars.registerHelper('rollField', (value) =>
    game.i18n.format('salvage-union.sheets.actor.roll-field', { field: value }),
  )
  Handlebars.registerHelper('wrapWithP', (value) => wrapWithP(value))
  Handlebars.registerHelper('displayCost', (cost, unit) =>
    [cost, unit].join(''),
  )

  Handlebars.registerHelper('displayTraits', (traits, uses) => {
    const displayTraits = [...traits]
    if (parseInt(uses?.max) > 0) {
      displayTraits.push(usesTraitDisplay(uses?.value))
    }
    return displayTraits.join(' // ')
  })
}

export const registerSystemSettings = () => {
  game.settings.register('salvage-union', 'systemMigrationVersion', {
    config: false,
    scope: 'world',
    type: String,
    default: '',
  })

  game.settings.register('salvage-union', 'displayMechScrapTotal', {
    config: true,
    scope: 'world',
    name: 'SETTINGS.displayMechScrapTotal.label',
    hint: 'SETTINGS.displayMechScrapTotal.hint',
    type: Boolean,
    default: true,
  })

  game.settings.register('salvage-union', 'crawlerUpgradeScrapRequired', {
    config: true,
    scope: 'world',
    name: 'SETTINGS.crawlerUpgradeScrapRequired.label',
    hint: 'SETTINGS.crawlerUpgradeScrapRequired.hint',
    type: Number,
    default: 30,
  })

  game.settings.register('salvage-union', 'customStatuses', {
    config: true,
    scope: 'world',
    name: 'SETTINGS.customStatuses.label',
    hint: 'SETTINGS.customStatuses.hint',
    type: Boolean,
    default: true,
    onChange: foundry.utils.debouncedReload,
  })

  game.settings.register('salvage-union', 'moveItemsNotCopy', {
    config: true,
    scope: 'world',
    name: 'SETTINGS.moveItemsNotCopy.label',
    hint: 'SETTINGS.moveItemsNotCopy.hint',
    type: Boolean,
    default: false,
  })
}

export const setupEnrichers = () => {
  CONFIG.TextEditor.enrichers = CONFIG.TextEditor.enrichers.concat([
    {
      // RollTable
      pattern: /@DisplayTable\[(.+?)\](?:{(.+?)})?/gm,
      enricher: async (match) => await enrichDisplayTable(match),
    },
    {
      // RollTableAsc
      pattern: /@DisplayTableAsc\[(.+?)\](?:{(.+?)})?/gm,
      enricher: async (match) => await enrichDisplayTable(match, true),
    },
    {
      // Item
      pattern: /@InlineItem\[(.+?)\](?:{(.+?)})?/gm,
      enricher: async (match) => await enrichInlineItem(match),
    },
    {
      // Chassis
      pattern: /@InlineChassis\[(.+?)\](?:{(.+?)})?/gm,
      enricher: async (match) => await enrichInlineChassis(match),
    },
    {
      // NPC
      pattern: /@InlineNpc\[(.+?)\](?:{(.+?)})?/gm,
      enricher: async (match) => await enrichInlineNpc(match),
    },
    {
      // Costs
      pattern: /{cost=([\dX]+[EAX]P)}/gm,
      enricher: async (match) => await enrichCost(match),
    },
  ])
}
