import * as Helpers from './helpers.js'

export default class SalvageUnionActor extends Actor {
  static getDefaultArtwork(data) {
    return {
      img: CONFIG.SALVAGE.defaultTokens[data.type],
      texture: { src: CONFIG.SALVAGE.defaultTokens[data.type] },
    }
  }

  prepareData() {
    super.prepareData()

    this.system.inventory = {
      max: 0,
      value: 0,
      items: [],
    }

    switch (this.type) {
      case 'pilot':
        this._preparePilotData()
        break

      case 'mech':
        this._prepareMechData()
        break

      case 'union-crawler':
        this._prepareCrawlerData()
        break

      case 'npc':
        this._prepareNPCData()
        break

      case 'npc-mech':
        this._prepareNPCMechData()
        break

      case 'bio-titan':
        this._prepareBioTitanData()
        break

      case 'vehicle':
        this._prepareVehicleData()
        break

      default:
        break
    }
    this._sortInventory()

    if (this.system.traits) this._sortTraits()
  }

  _sortInventory() {
    this.system.inventory.items.sort((a, b) => a.sort - b.sort)

    let runningTotal = 0
    for (const item of this.system.inventory.items) {
      runningTotal += item.size
      item.overflow = runningTotal > this.system.inventory.max
    }
    this.system.inventory.value = this.system.inventory.items.reduce((a, b) => {
      return a + b.size
    }, 0)
  }

  _sortTraits() {
    this.system.traits = this.system.traits.sort((a, b) =>
      a.toLocaleLowerCase() > b.toLocaleLowerCase() ? 1 : -1,
    )
  }

  /**
   * Add an item to the Actor's inventory
   * @param {Item} item The item to add
   * @param {Number} [size=1] The number of slots to take up
   * @param {Boolean} [canDelete=true] Whether or not this item can be managed from the inventory tab.
   */
  _addToInventory(item, size = 1, canDelete = true) {
    this.system.inventory.items.push({
      id: item.id,
      name: item.name,
      type: item.type,
      description: item.system.description,
      salvageValue: item.system.salvageValue,
      size: size,
      techLevel: item.system.techLevel,
      canDelete: canDelete,
      sort: item.sort,
    })
  }

  _preparePilotData() {
    // Inventory
    this.system.inventory.max = this.system.inventorySlots

    // Abilities
    this.system.abilities = Helpers.filterItems(this.items, 'ability')
    this.system.abilities.forEach((item) => {
      if (item.system.inventorySlots > 0) {
        this._addToInventory(item, item.system.inventorySlots, false)
      }
    })

    // Equipment
    this.system.equipment = Helpers.filterItems(this.items, 'equipment')
    this.system.equipment.forEach((item) => {
      if (item.system.inventorySlots > 0) {
        this._addToInventory(item, item.system.inventorySlots, false)
      }
    })

    // Salvage takes up triple size for pilots
    this._prepareSalvage(3)
  }

  _prepareMechData() {
    let scrapTotal = 0
    // Chassis
    const chassis = Helpers.filterItems(this.items, 'chassis')[0]
    if (chassis) {
      if (this.type === 'mech') {
        this.system.inventory.max += chassis.system.cargo
        this.system.moduleSlots = chassis.system.moduleSlots
        this.system.systemSlots = chassis.system.systemSlots
        this.system.heat.max = chassis.system.heat
        this.system['energy-points'].max = chassis.system.ep
      }
      this.system.sp.max = chassis.system.sp
      scrapTotal += this.itemScrapSize(chassis)
    }
    this.system.chassis = chassis

    // Modules
    this.system.moduleSlotsUsed = 0
    this.system.modules = Helpers.filterItems(this.items, 'module')
    this.system.modules.forEach((item) => {
      this.system.moduleSlotsUsed += item.system.slots
      this.system.inventory.max += item.system.cargo
      scrapTotal += this.itemScrapSize(item)
    })

    // Systems
    this.system.systemSlotsUsed = 0
    this.system.systems = Helpers.filterItems(this.items, 'system')
    this.system.systems.forEach((item) => {
      this.system.systemSlotsUsed += item.system.slots
      this.system.inventory.max += item.system.cargo
      scrapTotal += this.itemScrapSize(item)
    })

    this.system.scrapTotal = scrapTotal

    // Salvage
    this._prepareSalvage()
  }

  _prepareCrawlerData() {
    this.system.systems = Helpers.filterItems(this.items, 'system')
    this.system.bays = Helpers.filterItems(this.items, 'crawler-bay')

    this._prepareSalvage()

    let totalInT1 = 0
    this.system.salvage.forEach((item) => {
      totalInT1 +=
        (item.system.salvageValue || 1) *
        this.techLevelToInt(item.system.techLevel)
    })
    this.system.inventory.upgradeEquivalent = Math.floor(
      totalInT1 / this.techLevelToInt(this.system.techLevel),
    )
  }

  _prepareVehicleData() {
    this.system.systems = Helpers.filterItems(this.items, 'system')
    this.system.modules = Helpers.filterItems(this.items, 'module')
    this.system.abilities = Helpers.filterItems(this.items, 'npc-ability')
    this._prepareSalvage()
  }

  _prepareNPCData() {
    this.system.equipment = Helpers.filterItems(this.items, 'equipment')
    this.system.abilities = Helpers.filterItems(this.items, 'npc-ability')
  }

  _prepareNPCMechData() {
    this._prepareMechData()
    this.system.abilities = Helpers.filterItems(this.items, 'npc-ability')
  }

  _prepareBioTitanData() {
    this._prepareNPCData()
    this._prepareSalvage()
  }

  _prepareSalvage(salvageSize = 1) {
    this.system.salvage = Helpers.filterItems(this.items, 'salvage')
    this.system.salvage.forEach((item) => {
      const totalSalvageSize = (item.system.salvageValue || 1) * salvageSize
      this._addToInventory(item, totalSalvageSize)
    })
  }

  _onCreateDescendantDocuments(
    embeddedName,
    documents,
    result,
    options,
    userId,
  ) {
    super._onCreateDescendantDocuments(
      embeddedName,
      documents,
      result,
      options,
      userId,
    )
    this.updateMaximums()
  }

  _onUpdateDescendantDocuments(
    embeddedName,
    documents,
    result,
    options,
    userId,
  ) {
    super._onUpdateDescendantDocuments(
      embeddedName,
      documents,
      result,
      options,
      userId,
    )
    this.updateMaximums()
  }

  _onDeleteDescendantDocuments(
    embeddedName,
    documents,
    result,
    options,
    userId,
  ) {
    super._onDeleteDescendantDocuments(
      embeddedName,
      documents,
      result,
      options,
      userId,
    )
    this.updateMaximums()
  }

  /**
   * Update the Maximum values for this Actor's stats
   */
  async updateMaximums() {
    if (this.type == 'mech') {
      let maxSp = 0,
        maxEp = 0,
        maxHeat = 0

      const chassis = this.items.find((item) => item.type == 'chassis')
      if (chassis) {
        maxSp = chassis.system.sp
        maxEp = chassis.system.ep
        maxHeat = chassis.system.heat
      }

      this.system = {
        ...this.system,
        sp: {
          value: Helpers.clamp(this.system.sp.value, maxSp),
          max: maxSp,
        },
        'energy-points': {
          value: Helpers.clamp(this.system['energy-points'].value, maxEp),
          max: maxEp,
        },
        heat: {
          value: Helpers.clamp(this.system.heat.value, maxHeat),
          max: maxHeat,
        },
      }
    }
  }

  itemScrapSize(item) {
    return this.techLevelToInt(item.system.techLevel) * item.system.salvageValue
  }

  techLevelToInt(techLevel) {
    switch (techLevel) {
      case 'T1':
        return 1
      case 'T2':
        return 2
      case 'T3':
        return 3
      case 'T4':
        return 4
      case 'T5':
        return 5
      case 'T6':
        return 6
      default:
        return 0
    }
  }
}
