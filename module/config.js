export const SALVAGE = {}

SALVAGE.techLevels = {
  T1: 'salvage-union.salvage.T1',
  T2: 'salvage-union.salvage.T2',
  T3: 'salvage-union.salvage.T3',
  T4: 'salvage-union.salvage.T4',
  T5: 'salvage-union.salvage.T5',
  T6: 'salvage-union.salvage.T6',
  TB: 'salvage-union.salvage.TB',
  N: 'salvage-union.salvage.N',
}

SALVAGE.statusTypes = {
  ACTIVE: 0,
  DAMAGED: 1,
  DESTROYED: 2,
}

SALVAGE.statusClasses = {
  0: 'active',
  1: 'damaged',
  2: 'destroyed',
}

SALVAGE.statusLabels = {
  0: 'salvage-union.status.active',
  1: 'salvage-union.status.damaged',
  2: 'salvage-union.status.destroyed',
}

SALVAGE.defaultTokens = {
  // Actors
  'bio-titan': 'systems/salvage-union/images/tokens/bio-titan.webp',
  mech: 'systems/salvage-union/images/tokens/mech.webp',
  npc: 'systems/salvage-union/images/tokens/pilot.webp',
  'npc-mech': 'systems/salvage-union/images/tokens/mech.webp',
  pilot: 'systems/salvage-union/images/tokens/pilot.webp',
  'union-crawler': 'systems/salvage-union/images/tokens/union-crawler.webp',
  // Items
  ability: 'systems/salvage-union/images/tokens/ability.webp',
  chassis: 'systems/salvage-union/images/tokens/chassis.webp',
  'crawler-bay': 'systems/salvage-union/images/tokens/crawler-bay.webp',
  equipment: 'systems/salvage-union/images/tokens/equipment.webp',
  module: 'systems/salvage-union/images/tokens/module.webp',
  'npc-ability': 'systems/salvage-union/images/tokens/ability.webp',
  salvage: 'systems/salvage-union/images/tokens/salvage.webp',
  system: 'systems/salvage-union/images/tokens/system.webp',
  vehicle: 'systems/salvage-union/images/tokens/vehicle.webp',
}

SALVAGE.crawlerTech = {
  1: { sp: 20 },
  2: { sp: 25 },
  3: { sp: 30 },
  4: { sp: 35 },
  5: { sp: 40 },
  6: { sp: 50 },
}

SALVAGE.abilityLevels = {
  core: 'salvage-union.sheets.items.level.core',
  advanced: 'salvage-union.sheets.items.level.advanced',
  legendary: 'salvage-union.sheets.items.level.legendary',
}

SALVAGE.actionTypes = {
  p: 'salvage-union.sheets.items.action.p',
  f: 'salvage-union.sheets.items.action.f',
  r: 'salvage-union.sheets.items.action.r',
  t: 'salvage-union.sheets.items.action.t',
  s: 'salvage-union.sheets.items.action.s',
  l: 'salvage-union.sheets.items.action.l',
  d: 'salvage-union.sheets.items.action.d',
}

SALVAGE.ranges = {
  close: 'salvage-union.sheets.items.range.close',
  medium: 'salvage-union.sheets.items.range.medium',
  long: 'salvage-union.sheets.items.range.long',
  far: 'salvage-union.sheets.items.range.far',
}

SALVAGE.itemsWithTable = [
  'ability',
  'chassis',
  'crawler-bay',
  'equipment',
  'module',
  'npc-ability',
  'system',
]

SALVAGE.itemsWithTraits = [
  'ability',
  'equipment',
  'module',
  'npc-ability',
  'system',
]
SALVAGE.itemsWithTechLevel = [
  'chassis',
  'equipment',
  'module',
  'salvage',
  'system',
]

SALVAGE.itemsWithDamage = ['equipment', 'module', 'npc-ability', 'system']
SALVAGE.itemsWithRange = [
  'ability',
  'equipment',
  'module',
  'npc-ability',
  'system',
]

SALVAGE.itemsWithAction = [
  'ability',
  'equipment',
  'module',
  'npc-ability',
  'system',
]

SALVAGE.itemsWithCost = ['ability', 'equipment', 'module', 'system']

SALVAGE.itemsWithUses = [
  'ability',
  'equipment',
  'module',
  'npc-ability',
  'system',
]

SALVAGE.npcHealthTypes = {
  hp: 'salvage-union.sheets.actor.hp',
  sp: 'salvage-union.sheets.actor.sp',
}

SALVAGE.permittedItems = {
  pilot: ['ability', 'equipment', 'salvage'],
  mech: ['chassis', 'system', 'module', 'salvage'],
  'union-crawler': ['system', 'crawler-bay', 'salvage'],
  npc: ['equipment', 'npc-ability'],
  'npc-mech': ['chassis', 'system', 'module', 'salvage', 'npc-ability'],
  'bio-titan': ['npc-ability', 'salvage'],
  vehicle: ['system', 'module', 'salvage'],
}

SALVAGE.statusEffects = [
  {
    id: 'vulnerable',
    label: 'salvage-union.effect.statusVulnerable',
    icon: 'systems/salvage-union/images/statusEffects/cracked-shield.webp',
  },
  {
    id: 'overheat',
    label: 'salvage-union.effect.statusOverHeat',
    icon: 'systems/salvage-union/images/statusEffects/heat-haze.webp',
  },
  {
    id: 'burn',
    label: 'salvage-union.effect.statusBurn',
    icon: 'icons/svg/fire.svg',
  },
  {
    id: 'fly',
    label: 'salvage-union.effect.statusFly',
    icon: 'icons/svg/wing.svg',
  },
  {
    id: 'hover',
    label: 'salvage-union.effect.statusHover',
    icon: 'icons/svg/upgrade.svg',
  },
  {
    id: 'immobile',
    label: 'salvage-union.effect.statusImmobile',
    icon: 'systems/salvage-union/images/statusEffects/cement-shoes.webp',
  },
  {
    id: 'pinned',
    label: 'salvage-union.effect.statusPinned',
    icon: 'systems/salvage-union/images/statusEffects/missile-swarm.webp',
  },
  {
    id: 'prone',
    label: 'salvage-union.effect.statusProne',
    icon: 'icons/svg/falling.svg',
  },
  {
    id: 'destroyed',
    label: 'salvage-union.effect.statusDestroyed',
    icon: 'icons/svg/skull.svg',
  },
]
