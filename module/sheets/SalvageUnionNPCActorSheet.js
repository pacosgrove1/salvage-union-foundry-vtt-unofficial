import SalvageUnionActorSheet from './SalvageUnionActorSheet.js'

export default class SalvageUnionNPCActorSheet extends SalvageUnionActorSheet {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      width: 500,
      height: 750,
    })
  }
}
