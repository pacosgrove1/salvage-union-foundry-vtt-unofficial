import { addTrait, removeTrait } from '../helpers.js'

export default class SalvageUnionItemSheet extends ItemSheet {
  get template() {
    return 'systems/salvage-union/templates/sheets/item-sheet.hbs'
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      width: 500,
      height: 750,
    })
  }

  async getData() {
    const item = this.item
    const enrich = async (html) => {
      if (html) {
        return await TextEditor.enrichHTML(html, {
          secrets: item.isOwner,
          async: true,
        })
      }
      return html
    }

    const context = super.getData()

    const itemType = this.item.type
    context.config = CONFIG.SALVAGE
    context.system = this.item.system

    const secrets = this.item.isOwner

    context.system.enrichedDescription = await enrich(
      context.system.description,
      secrets,
    )

    if (itemType === 'crawler-bay')
      context.system.enrichedNpcDescription = await enrich(
        context.system.npc_description,
        secrets,
      )

    context.hasCost = CONFIG.SALVAGE.itemsWithCost.includes(itemType)
    context.hasTechLevel = CONFIG.SALVAGE.itemsWithTechLevel.includes(itemType)
    context.hasDamage = CONFIG.SALVAGE.itemsWithDamage.includes(itemType)
    context.hasRange = CONFIG.SALVAGE.itemsWithRange.includes(itemType)
    context.hasAction = CONFIG.SALVAGE.itemsWithAction.includes(itemType)
    context.hasUses = CONFIG.SALVAGE.itemsWithUses.includes(itemType)

    context.hasTraits = CONFIG.SALVAGE.itemsWithTraits.includes(itemType)
    context.traits = this.item.system.traits

    context.inventorySlots = this.item.system.inventorySlots

    context.hasTable = CONFIG.SALVAGE.itemsWithTable.includes(itemType)

    context.permittedOwners = Object.keys(CONFIG.SALVAGE.permittedItems).filter(
      (key) => CONFIG.SALVAGE.permittedItems[key].includes(itemType),
    )

    return context
  }

  activateListeners(html) {
    if (this.isEditable) {
      if (this.item.isOwner) {
        if (CONFIG.SALVAGE.itemsWithTable.includes(this.item.type)) {
          const dragDrop = new DragDrop({
            dropSelector: '.salvage-union.sheet.item-sheet',
            callbacks: { drop: this._onDropTable.bind(this) },
          })
          dragDrop.bind(html[0])
          html
            .find('.rollable-table-clear')
            .click(this._onClearTable.bind(this))
        }

        if (CONFIG.SALVAGE.itemsWithTraits.includes(this.item.type)) {
          html.find('.add-trait').click(this._onTraitCreate.bind(this))
          html.find('.trait-name').keydown(this._onTraitCreate.bind(this))
          html.find('.remove-trait').click(this._onTraitDelete.bind(this))
        }
      }
    }

    super.activateListeners(html)
  }

  /**
   * Drop a rollable table into the sheet.
   * @param {Event} event
   */
  async _onDropTable(event) {
    event.preventDefault()
    let dropData
    try {
      dropData = JSON.parse(event.dataTransfer.getData('text/plain'))
    } catch (err) {
      return false
    }

    let updateData = {}
    switch (dropData?.type) {
      case 'RollTable':
        updateData = { table_id: dropData.uuid }
        break
      default:
        break
    }

    await this.item.update({
      system: {
        ...updateData,
      },
    })
  }

  /**
   * Clear the RollTable association with this item
   * @param {Event} event
   */
  _onClearTable(event) {
    event.preventDefault()
    this.item.update({ system: { table_id: null } })
  }

  /**
   * Add a Trait
   * @param {Event} event
   */
  _onTraitCreate(event) {
    const newTrait = $(event.currentTarget.closest('.traits'))
      .find('.trait-name')[0]
      .value.trim()

    if (event.type === 'keydown') {
      // Trigger the action on enter
      if (event.keyCode === 13) addTrait(this.item, newTrait)
    } else {
      // Fallback for other events calling this method
      addTrait(this.item, newTrait)
    }
  }

  /**
   * Remove a Trait from the list
   * @param {Event} event
   */
  _onTraitDelete(event) {
    event.preventDefault()
    const traitToRemove = event.currentTarget.dataset.trait
    removeTrait(this.item, traitToRemove)
  }
}
