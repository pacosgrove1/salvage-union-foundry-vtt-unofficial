import * as Dice from '../dice.js'
import { fieldToChat, basicChatMessage } from '../chat.js'
import { addTrait, clamp, enrich, removeTrait, wrapWithP } from '../helpers.js'

export default class SalvageUnionActorSheet extends ActorSheet {
  get template() {
    return `systems/salvage-union/templates/sheets/actors/${this.actor.type}-sheet.hbs`
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      width: 1000,
      height: 750,
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.tab-content',
          initial: 'main',
        },
      ],
    })
  }

  /**
   * Run validation on an Item before adding it to an Actor's sheet.
   * @param {Event} event The drop event - used for calling super
   * @param {Object} data Data about the dropped item
   * @returns
   */
  async _onDropItem(event, data) {
    if (data.type === 'Item') {
      const item = await fromUuid(data.uuid)
      const itemType = game.i18n.localize(
        `salvage-union.sheets.type.${item.type}`,
      )
      const actorType = game.i18n.localize(
        `salvage-union.sheets.type.${this.actor.type}`,
      )

      const permitted = CONFIG.SALVAGE.permittedItems[this.actor.type].includes(
        item.type,
      )
      if (permitted) {
        if (item.type === 'chassis' && this.actor.system.chassis) {
          ui.notifications.error(
            game.i18n.format('salvage-union.notifications.drop-item-max', {
              type: itemType,
            }),
          )
          return
        }
        await super._onDropItem(event, data)

        const moveNotCopy = game.settings.get(
          'salvage-union',
          'moveItemsNotCopy',
        )
        if (
          item.parent &&
          moveNotCopy &&
          item.parent.uuid !== this.actor.uuid
        ) {
          item.parent.deleteEmbeddedDocuments('Item', [item.id])
        }
        return
      } else {
        ui.notifications.error(
          game.i18n.format('salvage-union.notifications.drop-item-rejected', {
            item: itemType,
            actor: actorType,
          }),
        )
        return
      }
    }
    super._onDropItem(event, data)
  }

  /**
   * Context menu for editing and deleting items from the sheet
   */
  itemContextMenu = [
    {
      name: game.i18n.localize('salvage-union.common.edit'),
      icon: '<i class="fas fa-edit"></i>',
      callback: (element) => {
        let itemId = element.closest('.item-context-menu').data('item-id')
        this.actor.items.get(itemId).sheet.render(true)
      },
    },
    {
      name: game.i18n.localize('salvage-union.common.delete'),
      icon: '<i class="fas fa-trash"></i>',
      callback: (element) => {
        let itemId = element.closest('.item-context-menu').data('item-id')
        this.actor.deleteEmbeddedDocuments('Item', [itemId])
      },
    },
  ]

  async getData() {
    const context = super.getData()
    const secrets = this.actor.isOwner

    context.config = CONFIG.SALVAGE
    context.activeStatus = CONFIG.SALVAGE.statusTypes.ACTIVE

    context.system = this.actor.system

    context.system.enrichedDescription = await enrich(
      context.system.description,
      secrets,
    )

    if (['pilot'].includes(this.actor.type)) {
      context.system.enrichedBackground = await enrich(
        context.system.background,
        secrets,
      )
      context.system.enrichedAppearance = await enrich(
        context.system.appearance,
        secrets,
      )
    }

    if (context.system.abilities)
      for (const item of context.system.abilities) {
        item.system.enrichedDescription = await enrich(
          item.system.description,
          secrets,
        )
      }
    if (context.system.equipment)
      for (const item of context.system.equipment) {
        item.system.enrichedDescription = await enrich(
          item.system.description,
          secrets,
        )
      }
    if (context.system.systems)
      for (const item of context.system.systems) {
        item.system.enrichedDescription = await enrich(
          item.system.description,
          secrets,
        )
      }
    if (context.system.modules)
      for (const item of context.system.modules) {
        item.system.enrichedDescription = await enrich(
          item.system.description,
          secrets,
        )
      }
    if (context.system.bays)
      for (const item of context.system.bays) {
        item.system.enrichedDescription = await enrich(
          item.system.description,
          secrets,
        )
        item.system.enrichedNpcDescription = await enrich(
          item.system.npc_description,
          secrets,
        )
      }
    if (context.system.chassis)
      context.system.chassis.system.enrichedDescription = await enrich(
        context.system.chassis.system.description,
        secrets,
      )
    if (context.system.inventory.items)
      for (const item of context.system.inventory.items) {
        item.enrichedDescription = await enrich(item.description, secrets)
      }

    context.displayMechScrapTotal =
      this.actor.type === 'mech'
        ? game.settings.get('salvage-union', 'displayMechScrapTotal')
        : false

    context.crawlerUpgradeScrapRequired =
      this.actor.type === 'union-crawler'
        ? game.settings.get('salvage-union', 'crawlerUpgradeScrapRequired')
        : 0

    return context
  }

  activateListeners(html) {
    if (this.isEditable) {
      if (this.actor.isOwner) {
        new ContextMenu(html, '.item-context-menu', this.itemContextMenu)

        html.find('.rollable-table-roll').click(this._rollItemTable.bind(this))
        html.find('.item .uses').click(this._decreaseUses.bind(this))
        html.find('.item .uses').contextmenu(this._resetUses.bind(this))

        if (this.actor.type === 'mech') {
          html.find('.capacityRoll').click(this._rollHeat.bind(this))
        }
        if (
          ['mech', 'npc-mech', 'union-crawler', 'vehicle'].includes(
            this.actor.type,
          )
        ) {
          html.find('.toggleDamage').click(this._toggleDamage.bind(this))
        }
        if (this.actor.type === 'union-crawler') {
          html
            .find('.crawler-npc-hp')
            .click(this._adjustCrawlerNpcHealth.bind(this))
        }

        if (['pilot', 'mech'].includes(this.actor.type)) {
          html.find('.sendFieldToChat').click(this._sendFieldToChat.bind(this))
          html.find('.rollProfile').click(this._rollProfileField.bind(this))
          html.find('.item-cost').click(this._subtractCost.bind(this))
        }

        html.find('.add-salvage').click(this._addSalvage.bind(this))
      }
    }

    if (['npc', 'vehicle'].includes(this.actor.type)) {
      html.find('.add-trait').click(this._onTraitCreate.bind(this))
      html.find('.trait-name').keydown(this._onTraitCreate.bind(this))
      html.find('.remove-trait').click(this._onTraitDelete.bind(this))
    }

    html.find('.sendToChat').click(this._sendItemToChat.bind(this))

    html.find('.item-name').click(this._toggleItem.bind(this))

    super.activateListeners(html)
  }

  /**
   * Roll on the table associated with an Item
   * @param {Event} event The triggering event
   */
  _rollItemTable(event) {
    event.preventDefault()
    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    Dice.rollItemTable(item)
  }

  /**
   * Post an item into the Chat
   * @param {Event} event The triggering event
   */
  _sendItemToChat(event) {
    event.preventDefault()
    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    item.toChat()
  }

  _sendFieldToChat(event) {
    event.preventDefault()
    const field = event.currentTarget.dataset.fieldName
    const value = this.actor.system[field]

    if (!value) return

    fieldToChat(
      game.i18n.localize(`salvage-union.sheets.actor.pilot.${field}`),
      value,
    )
  }

  /**
   * Roll a d20 vs. current Heat and post the result
   * @param {Event} event The triggering event
   */
  _rollHeat(event) {
    event.preventDefault()
    const target = event.currentTarget.dataset
    Dice.heatRoll(target.value)
  }

  /**
   * Change a mech's System or Module repair status
   * @param {Event} event The triggering event
   */
  _toggleDamage(event) {
    event.preventDefault()
    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    let newStatus =
      (item.system.status || CONFIG.SALVAGE.statusTypes.ACTIVE) + 1
    if (newStatus > CONFIG.SALVAGE.statusTypes.DESTROYED)
      newStatus = CONFIG.SALVAGE.statusTypes.ACTIVE
    item.update({
      system: {
        status: newStatus,
      },
    })
  }

  /**
   * Change a Crawler NPC's health
   * @param {Event} event The triggering event
   */
  _adjustCrawlerNpcHealth = (event) => {
    event.preventDefault()
    const dataset = event.currentTarget.dataset
    const change = parseInt(dataset.change)

    const item = this.actor.items.get(event.currentTarget.dataset.itemId)
    if (!item) return

    const value = item.system.npc_health.value + change
    const max = item.system.npc_health.max

    item.update({
      system: {
        npc_health: {
          value: clamp(value, max),
        },
      },
    })
  }

  /**
   * Remove an Item from the Actor
   * @param {Event} event The triggering event
   */
  _deleteItem(event) {
    event.preventDefault()
    const dataset = event.currentTarget.dataset
    let itemId = dataset.itemId
    this.actor.deleteEmbeddedDocuments('Item', [itemId])
  }

  /**
   * Add a Trait
   * @param {Event} event
   */
  _onTraitCreate(event) {
    const newTrait = $(event.currentTarget.closest('.traits'))
      .find('.trait-name')[0]
      .value.trim()

    if (event.type === 'keydown') {
      // Trigger the action on enter
      if (event.keyCode === 13) addTrait(this.actor, newTrait)
    } else {
      // Fallback for other events calling this method
      addTrait(this.actor, newTrait)
    }
  }

  /**
   * Remove a Trait from the list
   * @param {Event} event
   */
  _onTraitDelete(event) {
    event.preventDefault()
    const traitToRemove = event.currentTarget.dataset.trait
    removeTrait(this.actor, traitToRemove)
  }

  /**
   * Add an empty Salvage entry
   * @param {Event} event
   */
  async _addSalvage(event) {
    event.preventDefault()
    const itemType = 'salvage'
    const defaultData = {
      techLevel: 'T1',
    }

    const itemData = {
      name: game.i18n.localize('salvage-union.sheets.new-salvage'),
      type: itemType,
      data: {
        description: game.i18n.localize(
          'salvage-union.sheets.new-salvage-description',
        ),
        ...defaultData,
      },
    }

    const docs = await this.actor.createEmbeddedDocuments('Item', [itemData])

    docs.forEach((salvage) => salvage.sheet.render(true))
  }

  async _rollProfileField(event) {
    event.preventDefault()
    const fieldName = event.currentTarget.dataset.field
    let result = ''

    switch (fieldName) {
      case 'motto':
        result = await this._rollTable('TnCUB0IpTOeKTcUD')
        break
      case 'keepsake':
        result = await this._rollTable('oNmW8N3ZZRefBEQB')
        break
      case 'background':
        result = await this._rollTable('qLjj3SFXVKK2FAVg')
        break
      case 'quirk':
        result = await this._rollTable('yzNQCYbXgsZV8Fdi')
        break
      case 'appearance':
        result = await this._rollTable('HHDgxAYMKLysRS9k')
        break
      default:
        break
    }

    this.actor.update({
      system: {
        [fieldName]: result,
      },
    })
  }

  async _rollTable(tableId) {
    const table = await fromUuid(`Compendium.salvage-union.tables.${tableId}`)
    const result = await table.draw({ displayChat: false })

    return result?.results[0]?.text
  }

  async _subtractCost(event) {
    event.preventDefault()
    const { cost } = event.currentTarget.dataset
    const itemId = event.currentTarget.closest('.item')?.dataset.itemId
    const item = this.actor.items.get(itemId)

    const [fullCost, costNumber, costDenomination] =
      cost.match(/([\dX])([EAX]P)/)

    const typeToSubtract =
      costDenomination === 'AP' ? 'ability-points' : 'energy-points'

    const currentValue = this.actor.system[typeToSubtract].value

    if (currentValue < parseInt(costNumber)) {
      ui.notifications.error(
        game.i18n.format('salvage-union.dialog.spendCost.error', {
          denomination: costDenomination,
        }),
      )
      return
    }

    const data = await this.fetchCostData(
      fullCost,
      costNumber,
      costDenomination,
      item,
      currentValue,
    )
    if (data.cancelled) return
    if (data.cost > currentValue) {
      ui.notifications.error(
        game.i18n.format('salvage-union.dialog.spendCost.error', {
          denomination: costDenomination,
        }),
      )
      return
    }

    basicChatMessage(
      wrapWithP(
        game.i18n.format('salvage-union.chat.spendMessage', {
          actorId: this.actor.uuid,
          cost: `${data.cost}${costDenomination}`,
          itemId: item.uuid,
        }),
      ),
    )

    this.actor.update({
      system: {
        [typeToSubtract]: {
          value: currentValue - data.cost,
        },
      },
    })
  }

  async fetchCostData(fullCost, costNumber, costDenomination, item, maxSpend) {
    const template =
      'systems/salvage-union/templates/dialog/subtract-cost-dialog.hbs'

    const label = game.i18n.format('salvage-union.dialog.spendCost.label', {
      cost: fullCost,
      ability:
        item?.name ||
        game.i18n.localize('salvage-union.dialog.spendCost.placeholderItem'),
    })

    const requireInput = costNumber === 'X'

    const html = await renderTemplate(template, {
      label: label,
      maxSpend: maxSpend,
      costNumber: requireInput ? 1 : costNumber,
      costDenomination: costDenomination,
      requireInput: requireInput,
    })

    const type = game.i18n.localize(`salvage-union.sheets.type.${item?.type}`)
    const title = game.i18n.format('salvage-union.dialog.spendCost.title', {
      type: type,
    })

    return new Promise((resolve) => {
      const data = {
        title: title,
        content: html,
        buttons: {
          normal: {
            label: `<i class="fas fa-check"></i> ${game.i18n.localize(
              'salvage-union.dialog.submit',
            )}`,
            callback: (html) => resolve(this.processSpendCost(html)),
          },
          cancel: {
            label: `<i class="fas fa-times"></i> ${game.i18n.localize(
              'salvage-union.dialog.cancel',
            )}`,
            callback: (_html) => resolve({ cancelled: true }),
          },
        },
        default: 'normal',
        close: () => resolve({ cancelled: true }),
      }
      new Dialog(data, { classes: ['dialog', 'salvage-dialog'] }).render(true)
    })
  }

  processSpendCost(html) {
    const form = html[0].querySelector('form')
    return { cost: parseInt(form.cost.value) }
  }

  async _toggleItem(event) {
    event.preventDefault()
    const itemElement = event.currentTarget.closest('.item')
    const itemId = itemElement.dataset.itemId
    $(itemElement)
      .find('.drawer')
      .slideToggle({
        done: () => {
          this.toggleVisibility(itemId)
        },
      })
  }

  toggleVisibility(itemId) {
    const item = this.actor.items.get(itemId)

    if (item) {
      const visible = !item.system.visible ? false : true

      item.update({
        system: {
          visible: !visible,
        },
      })
    }
  }

  _decreaseUses(event) {
    event.preventDefault()
    const itemId = event.currentTarget.closest('.item').dataset.itemId
    const item = this.actor.items.get(itemId)
    const newValue = clamp(item.system.uses.value - 1, item.system.uses.max)
    item.update({
      system: {
        uses: {
          value: newValue,
        },
      },
    })
  }

  _resetUses(event) {
    event.preventDefault()
    const itemId = event.currentTarget.closest('.item').dataset.itemId
    const item = this.actor.items.get(itemId)
    item.update({
      system: {
        uses: {
          value: item.system.uses.max,
        },
      },
    })
  }
}
