export default class SalvageUnionJournalSheet extends JournalSheet {
  static get defaultOptions() {
    let classes = [
      'sheet journal-sheet journal-entry salvage-union-journal-sheet',
    ]
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: classes,
    })
  }
}
