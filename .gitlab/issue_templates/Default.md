- **Foundry version & build:** e.g., v11.301
- **System version:** e.g., 0.2.6
- **Browser:** (If applicable)
- **Permission level:** Gamemaster / Player / Both

**Description**

Including reproduction steps, if applicable.
